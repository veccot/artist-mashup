# Artist Mashup

### Run instructions

- Clone project
- Requires Java SE 1.8 and Maven
- Optional: Curl (via eg. GitBash or Cmder)

Run the following commands from the project root directory:

```sh
  $ mvn clean package 
  $ java -jar target/artist-mashup-0.1.0.jar
```

Spring will boot up on `localhost:8080`, and you can try to call it with the following `curl` command:

```sh
  $ curl -s -w "\nTotal time: %{time_total}\n" http://localhost:8080/artist/5b11f4ce-a62d-471e-81fc-a69a8278c7da
```
The first part of the `curl` command sets a format to write out the time to get the response. 
The second part is the GET request to /artist/\[mbid\].
