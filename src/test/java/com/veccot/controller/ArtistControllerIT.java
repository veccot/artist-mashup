package com.veccot.controller;

import com.veccot.response.Album;
import com.veccot.response.ArtistResponse;
import com.veccot.service.coverartarchive.CoverArt;
import com.veccot.service.musicbrainz.ArtistInfo;
import com.veccot.service.wikipedia.ArtistDescription;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.net.URL;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ArtistControllerIT {

    @LocalServerPort
    private int port;

    private URL base;

    @MockBean
    private RestTemplate restTemplate;

    @Autowired
    private TestRestTemplate template;

    private String MBID_NIRVANA = "5b11f4ce-a62d-471e-81fc-a69a8278c7da";
    private String WIKIPEDIA_RESOURCE = "https://en.wikipedia.org/wiki/Nirvana_(band)";
    private String MBID_NIRVANA_ALBUM = "1b022e01-4da6-387b-8658-8678046e4cef";

    @Before
    public void setUp() throws Exception {
        this.base = new URL("http://localhost:" + port + "/");
    }

    @Test
    public void getArtist_findsArtist_withDescription_withOneAlbum() throws Exception {
        ResponseEntity<ArtistInfo> responseEntity = new ArtistInfoResponseBuilder(HttpStatus.OK)
                .addRelation("wikipedia", WIKIPEDIA_RESOURCE)
                .addReleaseGroup("Nevermind", MBID_NIRVANA_ALBUM, "Album")
                .build();
        when(restTemplate.exchange(any(), eq(ArtistInfo.class))).thenReturn(responseEntity);

        ResponseEntity<ArtistDescription> artistDescription =
                ArtistDescriptionResponseHelper.createDescription(HttpStatus.OK, "description");
        when(restTemplate.exchange(any(), eq(ArtistDescription.class))).thenReturn(artistDescription);

        ResponseEntity<CoverArt> coverArt =
                new CoverArtResponseBuilder(HttpStatus.OK).addImage("http://image.png", true).build();
        when(restTemplate.exchange(any(), eq(CoverArt.class))).thenReturn(coverArt);

        ResponseEntity<ArtistResponse> response = template.getForEntity(base.toString() + "/artist/" + MBID_NIRVANA,
                ArtistResponse.class);

        assertThat(response.getBody(), notNullValue());
        assertThat(response.getBody().getMbid(), equalTo("5b11f4ce-a62d-471e-81fc-a69a8278c7da"));
        assertThat(response.getBody().getDescription(), equalTo("description"));
        assertThat(response.getBody().getAlbums(), hasSize(1));

        Album album = response.getBody().getAlbums().get(0);
        assertThat(album.getTitle(), equalTo("Nevermind"));
        assertThat(album.getId(), equalTo(MBID_NIRVANA_ALBUM));
        assertThat(album.getImage(), equalTo("http://image.png"));
    }
}
