package com.veccot.controller;

import com.veccot.service.coverartarchive.CoverArt;
import com.veccot.service.musicbrainz.ArtistInfo;
import com.veccot.service.wikipedia.ArtistDescription;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.client.RestTemplate;

import static org.hamcrest.Matchers.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode= DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ArtistControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private RestTemplate restTemplate;

    private String MBID_NIRVANA = "5b11f4ce-a62d-471e-81fc-a69a8278c7da";
    private String MBID_NOT_FOUND = "5b11f4ce-a62d-471e-81fc-a69a8278c7db";
    private String WIKIPEDIA_RESOURCE = "https://en.wikipedia.org/wiki/Nirvana_(band)";
    private String MBID_NIRVANA_ALBUM = "1b022e01-4da6-387b-8658-8678046e4cef";


    @Test
    public void getArtist_notValidMbid_ReturnsNotFound() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/artist/abc-123")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
        ;
    }

    @Test
    public void getArtist_validMbid_artistNotFound() throws Exception {
        ResponseEntity<ArtistInfo> artistInfo =
                new ArtistInfoResponseBuilder(HttpStatus.NOT_FOUND).build();
        when(restTemplate.exchange(any(), eq(ArtistInfo.class))).thenReturn(artistInfo);

        mvc.perform(MockMvcRequestBuilders.get("/artist/" + MBID_NOT_FOUND)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
        ;
    }

    @Test
    public void getArtist_findsArtist_descriptionNotFound() throws Exception {
        ResponseEntity<ArtistInfo> artistInfo =
                new ArtistInfoResponseBuilder(HttpStatus.OK)
                        .addRelation("wikipedia", WIKIPEDIA_RESOURCE)
                        .build();
        when(restTemplate.exchange(any(), eq(ArtistInfo.class))).thenReturn(artistInfo);

        ResponseEntity<ArtistDescription> artistDescription =
                ArtistDescriptionResponseHelper.createDescription(HttpStatus.NOT_FOUND);
        when(restTemplate.exchange(any(), eq(ArtistDescription.class))).thenReturn(artistDescription);

        mvc.perform(MockMvcRequestBuilders.get("/artist/" + MBID_NIRVANA)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.mbid", is(MBID_NIRVANA)))
                .andExpect(jsonPath("$.description", isEmptyString()))
                .andExpect(jsonPath("$.albums", hasSize(0)))
        ;
    }

    @Test
    public void getArtist_findsArtist_artistHasNotCreatedAnAlbum() throws Exception {
        ResponseEntity<ArtistInfo> artistInfo =
                new ArtistInfoResponseBuilder(HttpStatus.OK)
                        .addReleaseGroup("Nevermind", MBID_NIRVANA_ALBUM, "Single")
                        .build();
        when(restTemplate.exchange(any(), eq(ArtistInfo.class))).thenReturn(artistInfo);

        mvc.perform(MockMvcRequestBuilders.get("/artist/" + MBID_NIRVANA)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.mbid", is(MBID_NIRVANA)))
                .andExpect(jsonPath("$.description", isEmptyString()))
                .andExpect(jsonPath("$.albums", hasSize(0)))
        ;
    }

    @Test
    public void getArtist_findsArtist_withOneAlbum_CoverArtImageNotFound() throws Exception {
        ResponseEntity<ArtistInfo> artistInfo =
                new ArtistInfoResponseBuilder(HttpStatus.OK)
                        .addReleaseGroup("Nevermind", MBID_NIRVANA_ALBUM, "Album")
                        .build();
        when(restTemplate.exchange(any(), eq(ArtistInfo.class))).thenReturn(artistInfo);

        ResponseEntity<CoverArt> coverArt = new CoverArtResponseBuilder(HttpStatus.NOT_FOUND).build();
        when(restTemplate.exchange(any(), eq(CoverArt.class))).thenReturn(coverArt);

        mvc.perform(MockMvcRequestBuilders.get("/artist/" + MBID_NIRVANA)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("")))
                .andExpect(jsonPath("$.mbid", is(MBID_NIRVANA)))
                .andExpect(jsonPath("$.description", isEmptyString()))
                .andExpect(jsonPath("$.albums", hasSize(1)))
                .andExpect(jsonPath("$.albums[0].title", is("Nevermind")))
                .andExpect(jsonPath("$.albums[0].id", is(MBID_NIRVANA_ALBUM)))
                .andExpect(jsonPath("$.albums[0].image", isEmptyString()))
        ;
    }

    @Test
    public void getArtist_findsArtist_withDescription_withOneAlbum() throws Exception {
        String ARTIST_NAME = "Nirvana";
        ResponseEntity<ArtistInfo> artistInfo =
                new ArtistInfoResponseBuilder(HttpStatus.OK)
                        .addName(ARTIST_NAME)
                        .addRelation("wikipedia", WIKIPEDIA_RESOURCE)
                        .addReleaseGroup("Nevermind", MBID_NIRVANA_ALBUM, "Album")
                        .build();
        when(restTemplate.exchange(any(), eq(ArtistInfo.class))).thenReturn(artistInfo);

        ResponseEntity<ArtistDescription> artistDescription = ArtistDescriptionResponseHelper.createDescription(HttpStatus.OK, "description");
        when(restTemplate.exchange(any(), eq(ArtistDescription.class))).thenReturn(artistDescription);

        ResponseEntity<CoverArt> coverArt = new CoverArtResponseBuilder(HttpStatus.OK).addImage("http://image.png", true).build();
        when(restTemplate.exchange(any(), eq(CoverArt.class))).thenReturn(coverArt);

        mvc.perform(MockMvcRequestBuilders.get("/artist/" + MBID_NIRVANA)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(ARTIST_NAME)))
                .andExpect(jsonPath("$.mbid", is(MBID_NIRVANA)))
                .andExpect(jsonPath("$.description", is("description")))
                .andExpect(jsonPath("$.albums", hasSize(1)))
                .andExpect(jsonPath("$.albums[0].title", is("Nevermind")))
                .andExpect(jsonPath("$.albums[0].id", is(MBID_NIRVANA_ALBUM)))
                .andExpect(jsonPath("$.albums[0].image", is("http://image.png")))
        ;
    }

}