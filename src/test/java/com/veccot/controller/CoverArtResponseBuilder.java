package com.veccot.controller;

import com.veccot.service.coverartarchive.CoverArt;
import com.veccot.service.coverartarchive.Image;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

public class CoverArtResponseBuilder {
    private List<Image> images = new ArrayList<>();
    private HttpStatus status;

    public CoverArtResponseBuilder(HttpStatus status) {
        this.status = status;
    }

    public CoverArtResponseBuilder addImage(String image, boolean front){
        Image currentImage = new Image(image, front);
        images.add(currentImage);
        return this;
    }

    public ResponseEntity<CoverArt> build(){
        CoverArt response = new CoverArt(images);
        return new ResponseEntity<>(response, status);
    }
}
