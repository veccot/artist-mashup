package com.veccot.controller;

import com.veccot.service.wikipedia.ArtistDescription;
import com.veccot.service.wikipedia.Page;
import com.veccot.service.wikipedia.Pages;
import com.veccot.service.wikipedia.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public final class ArtistDescriptionResponseHelper {

    public static ResponseEntity<ArtistDescription> createDescription(HttpStatus status) {
        return createDescription(status, "");
    }

    public static ResponseEntity<ArtistDescription> createDescription(HttpStatus status, String description){
        Page page = new Page(description);
        Pages pages = new Pages();
        pages.setPagesItem("123", page);
        Query query = new Query(pages);
        ArtistDescription artistDescription = new ArtistDescription(query);
        return new ResponseEntity<>(artistDescription, status);
    }
}
