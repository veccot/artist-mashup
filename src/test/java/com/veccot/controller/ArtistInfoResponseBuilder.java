package com.veccot.controller;

import com.veccot.service.musicbrainz.ArtistInfo;
import com.veccot.service.musicbrainz.Relation;
import com.veccot.service.musicbrainz.ReleaseGroup;
import com.veccot.service.musicbrainz.Url;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

public class ArtistInfoResponseBuilder {
    private String name = "";
    private List<Relation> relations = new ArrayList<>();
    private List<ReleaseGroup> releaseGroups = new ArrayList<>();
    private HttpStatus status;

    public ArtistInfoResponseBuilder(HttpStatus status) {
        this.status = status;
    }

    public ArtistInfoResponseBuilder addName(String name){
        this.name = name;
        return this;
    }

    public ArtistInfoResponseBuilder addRelation(String type, String resource){
        Url url = new Url(resource);
        Relation relation = new Relation(type, url);
        relations.add(relation);
        return this;
    }

    public ArtistInfoResponseBuilder addReleaseGroup(String title, String id, String type){
        releaseGroups.add(new ReleaseGroup(title, id, type));
        return this;
    }

    public ResponseEntity<ArtistInfo> build(){
        ArtistInfo response = new ArtistInfo(name, relations, releaseGroups);
        return new ResponseEntity<>(response, status);
    }
}
