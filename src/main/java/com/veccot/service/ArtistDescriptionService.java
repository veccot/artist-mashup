package com.veccot.service;

import java.util.Optional;

public interface ArtistDescriptionService {
    Optional<String> getDesciption(String artistName);
}
