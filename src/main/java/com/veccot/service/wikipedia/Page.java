package com.veccot.service.wikipedia;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Page {

    @JsonProperty(value = "extract")
    private String description;

    public Page() {
    }

    public Page(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "Page{" +
                "description='" + description + '\'' +
                '}';
    }
}
