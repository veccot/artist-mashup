package com.veccot.service.wikipedia;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ArtistDescription {
    private Query query;

    public ArtistDescription() {
    }

    public ArtistDescription(Query query) {
        this.query = query;
    }

    public Query getQuery() {
        return query;
    }

    @Override
    public String toString() {
        return "ArtistDescription{" +
                "query=" + query +
                '}';
    }
}
