package com.veccot.service.wikipedia;

import com.veccot.service.ArtistDescriptionService;
import com.veccot.service.musicbrainz.ArtistInfoServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Optional;

import static java.lang.String.format;

@Service
public class ArtistDescriptionServiceImpl implements ArtistDescriptionService {

    private static final Logger log = LoggerFactory.getLogger(ArtistInfoServiceImpl.class);

    private final RestTemplate restTemplate;

    private static final String API_WIKIPEDIA = "https://en.wikipedia.org/w/api.php?action=query&format=json&prop=extracts&exintro=true&redirects=true&titles=%s";

    @Autowired
    public ArtistDescriptionServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public Optional<String> getDesciption(String artistName) {
        RequestEntity request = createRequest(artistName);
        Optional<ResponseEntity<ArtistDescription>> response = sendRequest(request);

        if (response.isPresent()) {
            if (response.get().getStatusCode().is2xxSuccessful()) {
                return getDescription(response.get().getBody());
            }
            log.warn("Could not get response body; Status code: " + response.get().getStatusCode());
        }
        return Optional.empty();
    }

    private Optional<String> getDescription(ArtistDescription response) {
        String description = response.getQuery().getPages().getFirstPage().getDescription();
        return Optional.ofNullable(description);
    }

    private RequestEntity createRequest(String artistName) {
        URI uri = URI.create(format(API_WIKIPEDIA, artistName));
        return RequestEntity.get(uri).accept(MediaType.APPLICATION_JSON).build();
    }

    private Optional<ResponseEntity<ArtistDescription>> sendRequest(RequestEntity request) {
        try {
            ResponseEntity<ArtistDescription> response = restTemplate.exchange(request, ArtistDescription.class);
            return Optional.ofNullable(response);
        } catch (RestClientException e) {
            log.warn("Could not get artist description; Reason: " + e.getMessage());
            return Optional.empty();
        }
    }
}
