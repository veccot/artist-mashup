package com.veccot.service.wikipedia;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Query {

    private Pages pages;

    public Query() {
    }

    public Query(Pages pages) {
        this.pages = pages;
    }

    public Pages getPages() {
        return pages;
    }

    @Override
    public String toString() {
        return "Query{" +
                "pages=" + pages +
                '}';
    }
}
