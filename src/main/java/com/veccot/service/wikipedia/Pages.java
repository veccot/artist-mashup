package com.veccot.service.wikipedia;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.HashMap;
import java.util.Map;

public class Pages {

    @JsonIgnore
    private Map<String, Page> pagesItems = new HashMap<>();

    public Pages() {
    }

    @JsonAnyGetter
    public Map<String, Page> getPagesItem() {
        return this.pagesItems;
    }

    @JsonAnySetter
    public void setPagesItem(String name, Page value) {
        this.pagesItems.put(name, value);
    }

    public Page getFirstPage(){
        return pagesItems.values().stream()
                .findFirst()
                .orElse(new Page(""));
    }

    @Override
    public String toString() {
        return "Pages{" +
                "pagesItems=" + pagesItems +
                '}';
    }
}
