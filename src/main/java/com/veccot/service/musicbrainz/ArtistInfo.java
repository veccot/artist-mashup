package com.veccot.service.musicbrainz;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ArtistInfo {

    private String name;
    private List<Relation> relations;
    @JsonProperty("release-groups")
    private List<ReleaseGroup> releaseGroups;

    public ArtistInfo() {
    }

    public ArtistInfo(String name, List<Relation> relations, List<ReleaseGroup> releaseGroups) {
        this.name = name;
        this.relations = relations;
        this.releaseGroups = releaseGroups;
    }

    public String getName() {
        return name;
    }

    public List<Relation> getRelations() {
        return relations;
    }

    public List<ReleaseGroup> getReleaseGroups() {
        return releaseGroups;
    }

    @Override
    public String toString() {
        return "ArtistInfo{" +
                "name='" + name + '\'' +
                ", relations=" + relations +
                ", releaseGroups=" + releaseGroups +
                '}';
    }
}
