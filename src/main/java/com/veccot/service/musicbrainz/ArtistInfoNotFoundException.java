package com.veccot.service.musicbrainz;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ArtistInfoNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public ArtistInfoNotFoundException(String message) {
        super(message);
    }
}
