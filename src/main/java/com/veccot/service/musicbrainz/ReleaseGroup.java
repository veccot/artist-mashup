package com.veccot.service.musicbrainz;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ReleaseGroup {

    private String title;
    private String id;
    @JsonProperty(value = "primary-type")
    private String type;

    public ReleaseGroup() {
    }

    public ReleaseGroup(String title, String id, String type) {
        this.title = title;
        this.id = id;
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return "ReleaseGroup{" +
                "title='" + title + '\'' +
                ", id='" + id + '\'' +
                ", type='" + type + '\'' +
                '}';
    }

}
