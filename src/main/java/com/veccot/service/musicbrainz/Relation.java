package com.veccot.service.musicbrainz;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Relation {
    private String type;
    private Url url;

    public Relation() {
    }

    public Relation(String type, Url url) {
        this.type = type;
        this.url = url;
    }

    public String getType() {
        return type;
    }

    public Url getUrl() {
        return url;
    }

    @Override
    public String toString() {
        return "Relation{" +
                "type='" + type + '\'' +
                ", url=" + url +
                '}';
    }
}
