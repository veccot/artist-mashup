package com.veccot.service.musicbrainz;

import com.veccot.service.ArtistInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

import static java.lang.String.format;

@Service
public class ArtistInfoServiceImpl implements ArtistInfoService {

    private static final Logger log = LoggerFactory.getLogger(ArtistInfoServiceImpl.class);

    private static final String API_MUSICBRAINZ = "http://musicbrainz.org/ws/2/artist/%s?&fmt=json&inc=url-rels+release-groups";

    private RestTemplate restTemplate;

    @Autowired
    public ArtistInfoServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public ArtistInfo getArtistInfo(String mbid) {
        RequestEntity request = createRequest(mbid);
        ResponseEntity<ArtistInfo> response = sendRequest(request);

        if (response.getStatusCode().is2xxSuccessful()){
            return response.getBody();
        }
        String message = "Could not get response body; Status code: " + response.getStatusCode();
        log.warn(message);
        throw new ArtistInfoNotFoundException(message);
    }

    private RequestEntity createRequest(String mbid) {
        URI uri = URI.create(format(API_MUSICBRAINZ, mbid));
        return RequestEntity.get(uri).accept(MediaType.APPLICATION_JSON).build();
    }

    private ResponseEntity<ArtistInfo> sendRequest(RequestEntity request) {
        try {
            return restTemplate.exchange(request, ArtistInfo.class);
        } catch (RestClientException e) {
            String message = "Could not get artist info; Reason: " + e.getMessage();
            log.warn(message);
            throw new ArtistInfoNotFoundException(message);
        }
    }
}
