package com.veccot.service.musicbrainz;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Url {
    private String resource;

    public Url() {
    }

    public Url(String resource) {
        this.resource = resource;
    }

    public String getResource() {
        return resource;
    }

    @Override
    public String toString() {
        return "Url{" +
                "resource='" + resource + '\'' +
                '}';
    }
}
