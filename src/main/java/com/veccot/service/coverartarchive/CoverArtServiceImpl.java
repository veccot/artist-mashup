package com.veccot.service.coverartarchive;

import com.veccot.service.CoverArtService;
import com.veccot.service.musicbrainz.ArtistInfoServiceImpl;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import static java.lang.String.format;

@Service
public class CoverArtServiceImpl implements CoverArtService {

    private static final Logger log = LoggerFactory.getLogger(ArtistInfoServiceImpl.class);

    private final RestTemplate restTemplate;

    private static final String API_COVERARTARCHIVE = "http://coverartarchive.org/release-group/%s";

    @Autowired
    public CoverArtServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public Map<String, String> getCoverArt(List<String> mbids) {
        Map<String, String> coverArt = new ConcurrentHashMap<>();
        Observable.fromIterable(mbids)
                .flatMap(val -> Observable.just(val)
                        .subscribeOn(Schedulers.computation())
                        .map(mbid -> new AbstractMap.SimpleEntry<>(mbid, getCoverArt(mbid)))
                ).blockingSubscribe(val -> coverArt.put(val.getKey(), val.getValue()));
        return coverArt;
    }

    private String getCoverArt(String mbid) {
        RequestEntity request = createRequest(mbid);
        Optional<ResponseEntity<CoverArt>> response = sendRequest(request, mbid);

        if (response.isPresent()) {
            if (response.get().getStatusCode().is2xxSuccessful()) {
                return getImage(response.get().getBody()).orElse("");
            }
            log.warn("Could not get response body for [" + mbid + "]; Status code: " + response.get().getStatusCode());
        }
        return "";
    }

    private Optional<String> getImage(CoverArt response) {
        return response.getImages().stream()
                .filter(Image::isFront)
                .map(Image::getImage)
                .findFirst();
    }

    private RequestEntity createRequest(String mbid) {
        URI uri = URI.create(format(API_COVERARTARCHIVE, mbid));
        return RequestEntity.get(uri).accept(MediaType.APPLICATION_JSON).build();
    }

    private Optional<ResponseEntity<CoverArt>> sendRequest(RequestEntity request, String mbid) {
        try {
            ResponseEntity<CoverArt> response = restTemplate.exchange(request, CoverArt.class);
            return Optional.ofNullable(response);
        } catch (RestClientException e) {
            log.warn("Could not get artist cover art for album mbid [" + mbid + "]; Reason: " + e.getMessage());
            return Optional.empty();
        }
    }
}
