package com.veccot.service.coverartarchive;

public class Image {

    private String image;
    private boolean front;

    public Image() {
    }

    public Image(String image, boolean front) {
        this.image = image;
        this.front = front;
    }

    public String getImage() {
        return image;
    }

    public boolean isFront() {
        return front;
    }

    @Override
    public String toString() {
        return "Image{" +
                "image='" + image + '\'' +
                ", front=" + front +
                '}';
    }
}
