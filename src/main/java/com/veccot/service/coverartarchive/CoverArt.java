package com.veccot.service.coverartarchive;

import java.util.List;

public class CoverArt {

    private List<Image> images;

    public CoverArt() {
    }

    public CoverArt(List<Image> images) {
        this.images = images;
    }

    public List<Image> getImages() {
        return images;
    }

    @Override
    public String toString() {
        return "CoverArt{" +
                "images=" + images +
                '}';
    }
}
