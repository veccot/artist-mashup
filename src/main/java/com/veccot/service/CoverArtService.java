package com.veccot.service;

import java.util.List;
import java.util.Map;

public interface CoverArtService {

    Map<String, String> getCoverArt(List<String> mbids);
}
