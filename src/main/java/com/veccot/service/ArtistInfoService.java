package com.veccot.service;

import com.veccot.service.musicbrainz.ArtistInfo;

public interface ArtistInfoService {
    ArtistInfo getArtistInfo(String mbid);
}
