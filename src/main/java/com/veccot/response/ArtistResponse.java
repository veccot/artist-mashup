package com.veccot.response;

import java.util.List;

public class ArtistResponse {
    private String name;
    private String mbid;
    private String description;
    private List<Album> albums;

    public ArtistResponse() {
    }

    public ArtistResponse(String name, String mbid, String description, List<Album> albums) {
        this.name = name;
        this.mbid = mbid;
        this.description = description;
        this.albums = albums;
    }

    public String getName() {
        return name;
    }

    public String getMbid() {
        return mbid;
    }

    public String getDescription() {
        return description;
    }

    public List<Album> getAlbums() {
        return albums;
    }

    @Override
    public String toString() {
        return "ArtistResponse{" +
                "name='" + name + '\'' +
                ", mbid='" + mbid + '\'' +
                ", description='" + description + '\'' +
                ", albums=" + albums +
                '}';
    }
}
