package com.veccot.facade;

import com.veccot.response.Album;
import com.veccot.response.ArtistResponse;
import com.veccot.service.ArtistDescriptionService;
import com.veccot.service.ArtistInfoService;
import com.veccot.service.CoverArtService;
import com.veccot.service.musicbrainz.ArtistInfo;
import com.veccot.service.musicbrainz.ReleaseGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

@Component
public class ArtistFacade {

    private final ArtistInfoService artistInfoService;
    private final ArtistDescriptionService artistDescriptionService;
    private final CoverArtService coverArtService;

    @Autowired
    public ArtistFacade(ArtistInfoService artistInfoService,
                        ArtistDescriptionService artistDescriptionService,
                        CoverArtService coverArtService) {
        this.artistInfoService = artistInfoService;
        this.artistDescriptionService = artistDescriptionService;
        this.coverArtService = coverArtService;
    }

    @Cacheable("artist")
    public ArtistResponse getArtistResponse(String mbid) {
        ArtistInfo artistInfo = artistInfoService.getArtistInfo(mbid);
        Optional<String> description = getDescription(artistInfo);
        List<Album> albums = getAlbums(artistInfo);

        return new ArtistResponse(artistInfo.getName(), mbid, description.orElse(""), albums);
    }

    private Optional<String> getDescription(ArtistInfo artistInfo) {
        Optional<String> artistName = getArtistName(artistInfo);
        return artistName.flatMap(artistDescriptionService::getDesciption);
    }

    private Optional<String> getArtistName(ArtistInfo artistInfo) {
        Optional<String> artistUrl = artistInfo.getRelations().stream()
                .filter(relation -> Objects.equals(relation.getType(), "wikipedia"))
                .map(relation -> relation.getUrl().getResource())
                .findFirst();
        return artistUrl.map(this::getArtistNameFromUrl);
    }

    private String getArtistNameFromUrl(String artistUrl) {
        return artistUrl.substring(artistUrl.lastIndexOf('/') + 1);
    }

    private List<Album> getAlbums(ArtistInfo artistInfo) {
        Map<String, String> albumTitles = getMbidToAlbumTitleMap(artistInfo);

        List<String> albumMbids = albumTitles.entrySet().stream().map(Map.Entry::getKey).collect(toList());
        Map<String, String> albumImages = coverArtService.getCoverArt(albumMbids);

        return createAlbums(albumTitles, albumImages);
    }

    private Map<String, String> getMbidToAlbumTitleMap(ArtistInfo artistInfo) {
        return artistInfo.getReleaseGroups().stream()
                .filter(releaseGroup -> Objects.equals(releaseGroup.getType(), "Album"))
                .collect(toMap(ReleaseGroup::getId, ReleaseGroup::getTitle));
    }

    private List<Album> createAlbums(Map<String, String> albumTitles, Map<String, String> albumImages) {
        return albumTitles.entrySet().stream()
                .map(titleMap -> new Album(
                        titleMap.getValue(),
                        titleMap.getKey(),
                        albumImages.getOrDefault(titleMap.getKey(), "")))
                .collect(toList());
    }
}
