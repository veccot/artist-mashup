package com.veccot.controller;

import com.veccot.facade.ArtistFacade;
import com.veccot.response.ArtistResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.regex.Pattern;

@RestController
public class ArtistController {

    private static final String MBID_FORMAT = "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}";

    private ArtistFacade artistFacade;

    @Autowired
    public ArtistController(ArtistFacade artistFacade) {
        this.artistFacade = artistFacade;
    }

    @CrossOrigin
    @GetMapping(value = "/artist/{mbid}")
    public ArtistResponse getArtist(@NotNull @PathVariable("mbid") String mbid) {
        validateMbid(mbid);
        return artistFacade.getArtistResponse(mbid);
    }

    private void validateMbid(String mbid) {
        System.out.println(mbid);
        final Pattern pattern = Pattern.compile(MBID_FORMAT);
        if (!pattern.matcher(mbid).matches()) {
            throw new InvalidMbidException("Invalid mbid format");
        }
    }
}
