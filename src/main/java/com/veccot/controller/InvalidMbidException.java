package com.veccot.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class InvalidMbidException extends RuntimeException {
    public InvalidMbidException(String message) {
        super(message);
    }
}
